# Hugo Base
## Introduction
This project is intended as a jumping-off point for future Hugo builds. It houses multiple branches featuring multiple CSS frameworks/versions of Bootstrap and TailwindCSS.  Be sure to check out the branch that matches the framework and version that you wish to use.  The master branch does not use any CSS frameworks by default.

### Installing Hugo
On macOS, install Homebrew and then run `brew install hugo` from the command line. Ideally, it should be accessible from your PATH for easy use.  See the [Hugo documentation](https://gohugo.io/getting-started/installing/) for more details.

Install the dependencies.  Note that this requires [Node.js](https://nodejs.org/) to run properly:

```sh
cd hugo-base
npm install
```

Run `hugo server -D` to start the local development server.

### Lighthouse CI
To run [Lighthouse CI](https://css-tricks.com/continuous-performance-analysis-with-lighthouse-ci-and-github-actions) tests locally: `lhci autorun`
