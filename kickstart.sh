#!/bin/bash

read -p "Enter the name of your new Hugo theme: "  theme
echo "Renaming the theme to $theme..."
echo "*******************************"

file="themes/base/theme.toml"

cat <<EOF > $file
name = "$theme"
license = "MIT"
licenselink = "https://github.com/yourname/yourtheme/blob/master/LICENSE"
description = ""
homepage = "http://example.com/"
tags = []
features = []
min_version = "0.83.1"

[author]
  name = ""
  homepage = ""

# If porting an existing theme
[original]
  name = ""
  homepage = ""
  repo = ""

EOF

mv themes/base themes/$theme

rm config.toml

file2="config.toml"

cat <<EOF > $file2
baseURL = "/"
languageCode = "en-us"
title = "Site Name"
theme = "$theme"
sectionPagesMenu = "main"
pluralizeListTitles = "false"
enableRobotsTXT = "false"
ignorefiles = [ "kickstart.sh","lighthouserc.js","README.md","composer.json","package*",".lighthouseci/*", "node_modules/.*" ]
disableKinds = []
disableHugoGeneratorInject = true

[params]
    foo = "Bar"
    
[menu]
    [[menu.main]]
        name = "Home"
        identifier = "home"
        url = "/"
        weight = 10
        
[minify]
    minifyOutput = "false"
        
[markup.goldmark.renderer]
    unsafe= true


EOF

echo "Done!"